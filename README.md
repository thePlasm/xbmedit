# xbmedit

An ncurses XBM graphics editor.

# usage

xbmedit [filename]

# controls

Arrow keys - move the cursor

Space - flips a pixel

H - positions the hotspot

S - saves the file

A - saves the file to a different location

# important note

The code is very messy (especially xbm_open.c).
I am currently in the process of cleaning it up (this also involves making documentation).