CC = gcc
CFLAGS = -Wall -Wpedantic -lncurses
SRCFILES = $(wildcard *.c)
OBJFILES = $(patsubst %.c, %.o, $(SRCFILES))

.PHONY: all clean

all: xbmedit

xbmedit: $(OBJFILES)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm *.o
	rm xbmedit
