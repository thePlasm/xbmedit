#include "xbm.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <libgen.h>

#define CHUNK_SIZE 8

struct chunk_node
{
        char str[CHUNK_SIZE];
        struct chunk_node *next;
};

/** This function prints error information stored in a flags variable as
 *  defined in xbm_parse.
 *  @param flags A flags variable as defined in xbm_parse
 *  @note This function prints to stderr.
 */
static void
xbm_flags_print(flags)
char flags;
{
        fprintf(stderr, "Error: Malformed XBM file:\n");
        if (!(flags & 0x80))
        {
                fprintf(stderr, "No bits information.\n");
        }
        if (!(flags & 0x40))
        {
                fprintf(stderr, "No width information.\n");
        }
        if (!(flags & 0x20))
        {
                fprintf(stderr, "No height information.\n");
        }
        if ((flags & 0x10) != (flags & 0x08) << 1)
        {
                fprintf(stderr, "Malformed hotspot information: ");
                if (flags & 0x08)
                {
                        fprintf(stderr, "No x_hot information.\n");
                }
                else
                {
                        fprintf(stderr, "No y_hot information.\n");
                }
        }
}

static int
ignore_whitespace(file_ptr)
FILE *file_ptr;
{
        int c;
        for (c = getc(file_ptr); isspace(c) && c != EOF; c = getc(file_ptr))
        {
                if (c == '\\')
                {
                        int p = getc(file_ptr);
                        if (!isspace(p))
                        {
                                if (ungetc(p, file_ptr) == EOF)
                                {
                                        fprintf(stderr, "Error: ungetc failed in "
                                                        "ignore_whitespace.\n");
                                        return 0;
                                }
                                return '\\';
                        }
                }
        }
        return c;
}

static void
chunks_free(head)
struct chunk_node *head;
{
        while (head != NULL)
        {
                struct chunk_node *temp = head;
                head = head->next;
                free(temp);
        }
}

static char
*stringify(head, length)
struct chunk_node *head;
size_t length;
{
        char *result;
        size_t i;
        size_t j = 0;
        if (head == NULL)
        {
                fprintf(stderr, "Error: NULL head in stringify.\n");
                return NULL;
        }
        result = malloc(length + 1);
        if (result == NULL)
        {
                fprintf(stderr, "Error: Failed to allocate memory for string "
                                "in stringify.\n");
                return NULL;
        }
        for (i = 0; i < length; ++i)
        {
                result[i] = head->str[j];
                ++j;
                if (j >= CHUNK_SIZE)
                {
                        struct chunk_node *temp = head;
                        if (head->next != NULL)
                        {
                                j = 0;
                                head = head->next;
                                free(temp);
                        }
                        else if (i != length - 1)
                        {
                                fprintf(stderr, "Error: Not enough chunks in "
                                                "stringify");
                                free(head);
                                free(result);
                                return NULL;
                        }

                }
        }
        free(head);
        if (i != length)
        {
                fprintf(stderr, "Error: Length mismatch in stringify.\n");
                free(result);
                return NULL;
        }
        result[length] = '\0';
        return result;
}

static int
get_token(file_ptr, token_ptr)
FILE *file_ptr;
char **token_ptr;
{
        struct chunk_node *head;
        struct chunk_node *tail;
        int p = EOF;
        int c;
        size_t length = 0;
        size_t i = 0;
        if (file_ptr == NULL || token_ptr == NULL)
        {
                fprintf(stderr, "Error: NULL arguments to get_token.\n");
                return 0;
        }
        c = ignore_whitespace(file_ptr);
        if (c == EOF)
        {
                *token_ptr = NULL;
                return EOF;
        }
        tail = head = malloc(sizeof(struct chunk_node));
        if (head == NULL)
        {
                fprintf(stderr, "Error: Failed to allocate memory for head in "
                                "get_token.\n");
                return 0;
        }
        while (c != EOF)
        {
                if (((p == '_' || isalnum(p)) && (c == '_' || isalnum(c))) ||
                    (p == '#' && (c == '_' || isalpha(c))) ||
                    p == EOF)
                {
                        tail->str[i] = c;
                        ++i;
                        ++length;
                        if (i >= CHUNK_SIZE)
                        {
                                tail->next = malloc(sizeof(struct chunk_node));
                                if (tail->next == NULL)
                                {
                                        chunks_free(head);
                                        fprintf(stderr, "Error: Failed to "
                                                        "allocate memory "
                                                        "for tail in get_token");
                                        return 0;
                                }
                                tail = tail->next;
                                i = 0;
                        }
                }
                else {
                        tail->next = NULL;
                        if (ungetc(c, file_ptr) == EOF)
                        {
                                fprintf(stderr, "Error: ungetc failed in "
                                                "get_token");
                                chunks_free(head);
                                return 0;
                        }
                        break;
                }
                p = c;
                c = getc(file_ptr);
        }
        *token_ptr = stringify(head, length);
        if (*token_ptr == NULL)
        {
                fprintf(stderr, "Error: stringify failed in get_token.\n");
                return 0;
        }
        return c == EOF ? EOF : 1;
}

static int
ignore_line_nl(file_ptr)
FILE *file_ptr;
{
        int c;
        for (c = getc(file_ptr);
             c != EOF && c != '\n';
             c = getc(file_ptr))
        {
                if (c == '\\')
                {
                        c = getc(file_ptr);
                }
        }
        return c;
}

static int
ignore_line_sc(file_ptr)
FILE *file_ptr;
{
        char dquoted = 0;
        char squoted = 0;
        int c;
        for (c = getc(file_ptr);
             c != EOF && (c != ';' || dquoted || squoted);
             c = getc(file_ptr))
        {
                switch (c)
                {
                case '\'':
                        dquoted = squoted ? 0 : !dquoted;
                        break;
                case '\"':
                        squoted = dquoted ? 0 : !squoted;
                        break;
                case '\\':
                        c = getc(file_ptr);
                        break;
                }
        }
        return c;
}

static int
ignore_line(file_ptr, token, filename)
FILE *file_ptr;
char *token;
char *filename;
{
        if (token == NULL || *token == '\0')
        {
                fprintf(stderr, "Error: NULL token in ignore_line.\n");
                return 0;
        }
        else if (*token == '#')
        {
                return ignore_line_nl(file_ptr);
        }
        else
        {
                return ignore_line_sc(file_ptr);
        }
}

static char
find_pre_type(token, filename)
char *token;
char *filename;
{
        size_t length = strlen(filename);
        if (strncmp(filename, token, length))
        {
                return 0;
        }
        if (!strcmp(token + length, "_width"))
        {
                return 1;
        }
        if (!strcmp(token + length, "_height"))
        {
                return 2;
        }
        if (!strcmp(token + length, "_x_hot"))
        {
                return 3;
        }
        if (!strcmp(token + length, "_y_hot"))
        {
                return 4;
        }
        return 0;
}

static int
process_pre_line(file_ptr, xbm_res, flags_ptr, filename)
FILE *file_ptr;
xbm_file *xbm_res;
char *flags_ptr;
char *filename;
{
        int status;
        char *token = NULL;
        char type;
        long val;
        if (file_ptr == NULL ||
            xbm_res == NULL ||
            flags_ptr == NULL ||
            filename == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in "
                                "process_pre_line.\n");
                return 0;
        }
        status = get_token(file_ptr, &token);
        if (status == 0 || status == EOF)
        {
                fprintf(stderr, "Error: get_token failed in "
                                "process_pre_line.\n");
                free(token);
                return 0;
        }
        type = find_pre_type(token, filename);
        free(token);
        token = NULL;
        status = get_token(file_ptr, &token);
        if (status == 0 || status == EOF)
        {
                fprintf(stderr, "Error: get_token failed in "
                                "process_pre_line.\n");
                free(token);
                return 0;
        }
        val = strtol(token, NULL, 0);
        switch (type)
        {
        case 1:
                *flags_ptr |= 0x40;
                xbm_res->width = val;
                break;
        case 2:
                *flags_ptr |= 0x20;
                xbm_res->height = val;
                break;
        case 3:
                *flags_ptr |= 0x10;
                xbm_res->x_hot = val;
                break;
        case 4:
                *flags_ptr |= 0x08;
                xbm_res->y_hot = val;
                break;
        }
        status = ignore_line_nl(file_ptr);
        status = ungetc(status, file_ptr);
        if (status == EOF)
        {
                fprintf(stderr, "Error: ungetc failed in process_pre_line.\n");
                return 0;
        }
        return 1;
}

static char
hex_lit(token)
char *token;
{
        if (token != NULL &&
            token[0] == '0' &&
            tolower(token[1]) == 'x' &&
            isxdigit(token[2]) &&
            isxdigit(token[3]) &&
            strlen(token) == 4)
        {
                return 1;
        }
        return 0;
}

static char
isstralnum(str)
char *str;
{
        size_t i;
        if (str == NULL)
        {
                fprintf(stderr, "Error: NULL str in isstralnum.\n");
        }
        for (i = 0; str[i] != '\0'; ++i)
        {
                if (!isalnum(str[i]))
                {
                        return 0;
                }
        }
        return 1;
}

static char
check_bits_token(token, ptoken, filename)
char *token;
char *ptoken;
char *filename;
{
        size_t length;
        char filename_like_token;
        char filename_like_ptoken;
        if (token == NULL || filename == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in "
                                "check_bits_token.\n");
                return 0;
        }
        if (ptoken == NULL)
        {
                if (!strcmp(token, "char") ||
                    !strcmp(token, "signed") ||
                    !strcmp(token, "unsigned"))
                {
                        return 1;
                }
                fprintf(stderr, "Error: NULL ptoken in wrong place in "
                                "check_bits_token.\n");
                return 0;
        }
        if (!strcmp(token, "char") && (!strcmp(ptoken, "signed") ||
                                       !strcmp(ptoken, "unsigned")))
        {
                return 1;
        }
        if (!strcmp(token, "*") && !strcmp(ptoken, "char"))
        {
                return 1;
        }
        length = strlen(filename);
        filename_like_token = !strncmp(token, filename, length) &&
                              !strcmp(token + length, "_bits");
        if (filename_like_token &&
            (!strcmp(ptoken, "char") || !strcmp(ptoken, "*")))
        {
                return 1;
        }
        filename_like_ptoken = !strncmp(ptoken, filename, length) &&
                               !strcmp(ptoken + length, "_bits");
        if (!strcmp(token, "[") && filename_like_ptoken)
        {
                return 1;
        }
        if (isstralnum(token) && !strcmp(ptoken, "["))
        {
                return 1;
        }
        if (!strcmp(token, "]") && (!strcmp(ptoken, "[") || isstralnum(ptoken)))
        {
                return 1;
        }
        if (!strcmp(token, "=") && (!strcmp(ptoken, "]") || filename_like_ptoken))
        {
                return 1;
        }
        if (!strcmp(token, "{") && !strcmp(ptoken, "="))
        {
                return 1;
        }
        if (hex_lit(token) && (!strcmp(ptoken, "{") || !strcmp(ptoken, ",")))
        {
                return 1;
        }
        if ((!strcmp(token, ",") || !strcmp(token, "}")) &&
            hex_lit(ptoken))
        {
                return 1;
        }
        if (!strcmp(token, ";") || !strcmp(ptoken, "}"))
        {
                return 1;
        }
        return 0;
}

static int
process_bits_line(file_ptr, xbm_res, flags_ptr, filename)
FILE *file_ptr;
xbm_file *xbm_res;
char *flags_ptr;
char *filename;
{
        char *token = NULL;
        char *ptoken = NULL;
        size_t width_bytes;
        size_t i = 0;
        char ast = 0;
        if (file_ptr == NULL ||
            xbm_res == NULL ||
            flags_ptr == NULL ||
            filename == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in "
                                "process_bits_line.\n");
                return 0;
        }
        if (!(*flags_ptr & 0x40) || !(*flags_ptr & 0x20))
        {
                fprintf(stderr, "Error: Width and height unknown in "
                                "process_bits_line.\n");
                return 0;
        }
        width_bytes = xbm_res->width / 8 + (xbm_res->width & 7 ? 1 : 0);
        xbm_res->length = width_bytes * xbm_res->height;
        xbm_res->bits = malloc(xbm_res->length);
        if (xbm_res->bits == NULL)
        {
                fprintf(stderr, "Error: Allocating memory for bits array "
                                "failed in process_bits_line.\n");
                return 0;
        }
        do
        {
                int status;
                free(ptoken);
                ptoken = token;
                token = NULL;
                status = get_token(file_ptr, &token);
                if (status == 0 || (status == EOF && strcmp(token, ";")))
                {
                        fprintf(stderr, "Error: get_token failed in "
                                        "process_bits_line.\n");
                        free(xbm_res->bits);
                        free(token);
                        free(ptoken);
                        return 0;
                }
                if ((ast && !strcmp(token, "[")) ||
                    !check_bits_token(token, ptoken, filename))
                {
                        status = ignore_line_sc(file_ptr);
                        status = ungetc(status, file_ptr);
                        free(token);
                        free(ptoken);
                        free(xbm_res->bits);
                        if (status == EOF)
                        {
                                fprintf(stderr, "Error: ungetc failed in "
                                                "process_bits_line.\n");
                                return 0;
                        }
                        return 1;
                }
                if (hex_lit(token))
                {
                        if (i < xbm_res->length)
                        {
                                xbm_res->bits[i] = strtol(token, NULL, 0);
                                ++i;
                        }
                }
                if (!strcmp(token, ";"))
                {
                        *flags_ptr |= 0x80;
                }
                if (!strcmp(token, "*"))
                {
                        ast = 1;
                }
        }
        while (!(*flags_ptr & 0x80));
        free(token);
        free(ptoken);
        return 1;
}

int
testeof(file_ptr)
FILE *file_ptr;
{
        int c;
        if (file_ptr == NULL)
        {
                fprintf(stderr, "Error: NULL file_ptr in testeof.\n");
                return 0;
        }
        c = getc(file_ptr);
        if (c == EOF)
        {
                return EOF;
        }
        if (ungetc(c, file_ptr) == EOF)
        {
                fprintf(stderr, "Error: ungetc failed in testeof.\n");
                return 0;
        }
        return 1;
}

/** This function will process a "line" in an XBM file.
 *  @param file_ptr A pointer to the XBM file as a FILE *
 *  @param xbm_res A pointer to an xbm_file struct, or NULL on error
 *  @param flags_ptr A pointer to a flags variable as defined in xbm_parse
 *  @return A status code as defined in xbm_parse
 */
static int
process_line(file_ptr, xbm_res, flags_ptr, filename)
FILE *file_ptr;
xbm_file *xbm_res;
char *flags_ptr;
char *filename;
{
        char *token = NULL;
        int status;
        if (file_ptr == NULL ||
            xbm_res == NULL ||
            flags_ptr == NULL ||
            filename == NULL)
        {
                fprintf(stderr, "Error: NULL arguments to process_line.\n");
                return 0;
        }
        status = get_token(file_ptr, &token);
        if (status == 0)
        {
                fprintf(stderr, "Error: get_token failed in process_line.\n");
                free(token);
                return 0;
        }
        if (status == EOF)
        {
                free(token);
                return EOF;
        }
        if (!strcmp(token, "#define"))
        {
                status = process_pre_line(file_ptr, xbm_res, flags_ptr, filename);
        }
        else if (!strcmp(token, "static"))
        {
                if (!(*flags_ptr & 0x80))
                {
                        status = process_bits_line(file_ptr, xbm_res, flags_ptr, filename);
                }
                else
                {
                        return testeof(file_ptr);
                }
        }
        else
        {
                status = ignore_line(file_ptr, token);
                if (status == EOF)
                {
                        fprintf(stderr, "Error: ignore_line encountered "
                                        "unexpected EOF in process_line.\n");
                        free(token);
                        return EOF;
                }
        }
        if (status == 0)
        {
                fprintf(stderr, "Error: process_line failed.\n");
                free(token);
                return 0;
        }
        return 1;
}

/** This function parses an open XBM file
 *  @param file_ptr A pointer to the XBM file as a FILE *
 *  @param filename A filename used only for parsing
 *  @return A pointer to an xbm_file struct, or NULL on error
 *  @note The pointer that is return needs to be freed
 */
static xbm_file
*xbm_parse(file_ptr, filename)
FILE *file_ptr;
char *filename;
{
        int status = 1; /**< Terminates parsing if EOF, 0 on error */
        /** This represents how much information needed in the XBM file has
         *  been parsed so far.
         *  @note In this description the bits are ordered MSB first.
         *  The first bit represents result->bits.
         *  The second bit represents result->width.
         *  The third bit represents result->height.
         *  The fourth bit represents result->x_hot.
         *  The fifth bit represent result->y_hot.
         *  The other bits should remain as 0.
         */
        char flags = 0;
        xbm_file *result;
        if (file_ptr == NULL || filename == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in xbm_parse.\n");
                return NULL;
        }
        result = malloc(sizeof(xbm_file));
        if (result == NULL)
        {
                fprintf(stderr,
                        "Error: Failed to allocate memory for xbm_file in "
                        "xbm_parse.\n");
                return NULL;
        }
        while (status != EOF && flags != 0xFC)
        {
                status = process_line(file_ptr, result, &flags, filename);
                if (!status)
                {
                        fprintf(stderr, "Error: process_line failed in "
                                        "xbm_parse.\n");
                        free(result->bits);
                        free(result);
                        return NULL;
                }
        }
        if ((flags & 0xE0) != 0xE0 || (flags & 0x10) != (flags & 0x08) << 1)
        {
                xbm_flags_print(flags);
                free(result->bits);
                free(result);
                return NULL;
        }
        result->has_hot = (flags & 0x10) ? true : false;
        return result;
}

static char
*strwolast(str, num)
char *str;
size_t num;
{
        size_t length;
        char *result;
        if (str == NULL)
        {
                fprintf(stderr, "Error: NULL str in strwolast.\n");
                return NULL;
        }
        length = strlen(str);
        result = malloc(length + 1 - num);
        if (result == NULL)
        {
                fprintf(stderr, "Error: malloc failed in strwolast.\n");
                return NULL;
        }
        memcpy(result, str, length - num);
        result[length - num] = '\0';
        return result;
}

/** This function opens and parses an XBM file
 *  @param filename The filename of the XBM file as a null-terminated string
 *  @return A pointer to an xbm_file struct, or NULL on error
 *  @note The pointer that is returned needs to be freed
 */
xbm_file
*xbm_open(filename)
char *filename;
{
        FILE *file_ptr;
        xbm_file *result;
        char *reduced_fn;
        if (filename == NULL)
        {
                fprintf(stderr, "Error: NULL filename in xbm_open.\n");
                return NULL;
        }
        if ((file_ptr = fopen(filename, "r")) == NULL)
        {
                perror("Error");
                return NULL;
        }
        reduced_fn = basename(filename);
        reduced_fn = strwolast(reduced_fn, 4);
        if (reduced_fn == NULL)
        {
                fprintf(stderr, "Error: strwolast failed in xbm_open.\n");
        }
        result = xbm_parse(file_ptr, reduced_fn);
        free(reduced_fn);
        if (result == NULL)
        {
                fprintf(stderr, "Error: xbm_parse failed in xbm_open.\n");
                if (fclose(file_ptr) == EOF)
                {
                        perror("Error");
                }
                return NULL;
        }
        if (fclose(file_ptr) == EOF)
        {
                perror("Error");
                return NULL;
        }
        return result;
}

static int
print_bits(file_ptr, bits, width, height, name)
FILE *file_ptr;
char *bits;
size_t width;
size_t height;
char *name;
{
        size_t y;
        size_t i = 0;
        if (file_ptr == NULL || bits == NULL || name == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in print_bits.\n");
                return 0;
        }
        fprintf(file_ptr, "static char %s_bits[] = {\n", name);
        for (y = 0; y < height; ++y)
        {
                size_t x;
                fputc('\t', file_ptr);
                for (x = 0; x < width; ++x)
                {
                        fprintf(file_ptr,
                                "0x%02hhX%s",
                                bits[i],
                                i < width * height - 1 ? ", " : "");
                        i++;
                }
                if (y < height - 1)
                {
                        fputc('\n', file_ptr);
                }
        }
        fprintf(file_ptr, "};");
        return 1;
}

int
xbm_save(canvas, name)
xbm_file *canvas;
char name[];
{
        FILE *file;
        char *reduced_fn;
        if (canvas == NULL || canvas->bits == NULL || name == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in xbm_save.\n");
                return 0;
        }
        reduced_fn = basename(name);
        reduced_fn = strwolast(reduced_fn, 4);
        file = fopen(name, "w");
        if (file == NULL)
        {
                perror("Error in xbm_save");
                return 0;
        }
        fprintf(file, "#define %s_width %lu\n", reduced_fn, canvas->width);
        fprintf(file, "#define %s_height %lu\n", reduced_fn, canvas->height);
        if (canvas->has_hot)
        {
                fprintf(file,
                        "#define %s_x_hot %lu\n",
                        reduced_fn,
                        canvas->x_hot);
                fprintf(file,
                        "#define %s_y_hot %lu\n",
                        reduced_fn,
                        canvas->y_hot);
        }
        print_bits(file,
                   canvas->bits,
                   canvas->length / canvas->height,
                   canvas->height,
                   reduced_fn);
        if (fclose(file) == EOF)
        {
                perror("Error in xbm_save");
                return 0;
        }
        return 1;
}
