#ifndef __XBM_H

#define __XBM_H

#include <stdlib.h>
#include <stdbool.h>

typedef
struct
{
        char *bits;
        size_t length;
        size_t width;
        size_t height;
        size_t x_hot;
        size_t y_hot;
        bool has_hot;
}
xbm_file;

xbm_file *xbm_open(char *filename);
int xbm_save(xbm_file *canvas, char name[]);

#endif
