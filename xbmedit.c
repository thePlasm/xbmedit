#include "xbm.h"
#include <stdio.h>
#include <curses.h>
#include <limits.h>
#include <string.h>

#define XBM_ERR 0
#define XBM_OK 1
#define XBM_CONT 2

#define DEFAULT_WIDTH 32
#define DEFAULT_HEIGHT 32

#define FULL_CHAR '#'
#define EMPTY_CHAR '.'

#define IN_BUF_MAX 20 /* used only for stdin parsing */
#define FN_BUF_MAX 100 /* used only for edit_a */

#define CURSOR_ATTR A_BLINK
#define HOT_ATTR A_STANDOUT

struct coord
{
        size_t x_pos;
        size_t y_pos;
};

void
free_xbm(file)
xbm_file *file;
{
        if (file != NULL)
        {
                free(file->bits);
                free(file);
        }
}

int
init_curses(void)
{
        initscr();
        if (cbreak() == ERR)
        {
                return XBM_ERR;
        }
        if (keypad(stdscr, TRUE) == ERR)
        {
                return XBM_ERR;
        }
        if (noecho() == ERR)
        {
                return XBM_ERR;
        }
        return XBM_OK;
}

int
kill_curses(void)
{
        if (endwin() == ERR)
        {
                return XBM_ERR;
        }
        return XBM_OK;
}

ssize_t
get_size(void)
{
        char in_buf[IN_BUF_MAX];
        ssize_t result;
        if (fgets(in_buf, IN_BUF_MAX, stdin) == NULL)
        {
                fprintf(stderr, "Error: fgets failed in get_size.\n");
                return -1;
        }
        if (*in_buf == '\n' || *in_buf == '\0')
        {
                return -2;
        }
        result = strtol(in_buf, NULL, 0);
        if (result == LONG_MAX || result == LONG_MIN)
        {
                perror("Error in get_size");
                return -1;
        }
        if (result < 0)
        {
                fprintf(stderr, "Error: Negative size in get_size.\n");
                return -1;
        }
        return result;
}

xbm_file
*init_prompt(void)
{
        xbm_file *result = malloc(sizeof(xbm_file));
        size_t width_bytes;
        ssize_t temp_width;
        ssize_t temp_height;
        if (result == NULL)
        {
                fprintf(stderr, "Error: Failed to allocate result in "
                                "init_prompt.\n");
                return NULL;
        }
        printf("New file:\n");
        printf("Please enter canvas width in px (%d): ", DEFAULT_WIDTH);
        temp_width = get_size();
        if (temp_width < 0)
        {
                result->width = DEFAULT_WIDTH;
        }
        else
        {
                result->width = temp_width;
        }
        printf("Please enter canvas height in px (%d): ", DEFAULT_HEIGHT);
        temp_height = get_size();
        if (temp_height < 0)
        {
                result->height = DEFAULT_HEIGHT;
        }
        else
        {
                result->height = temp_height;
        }
        width_bytes = result->width / 8 + ((result->width & 7) ? 1 : 0);
        result->length = width_bytes * result->height;
        result->bits = malloc(result->length);
        if (result->bits == NULL)
        {
                fprintf(stderr,
                        "Error: Failed to allocate result->bits in "
                        "init_prompt.\n");
                free(result);
                return NULL;
        }
        result->has_hot = false;
        return result;
}

int
draw_top(filename, canvas, offset_x, offset_y)
char filename[];
xbm_file *canvas;
size_t offset_x;
size_t offset_y;
{
        char *realname = (filename == NULL ? "Untitled" : filename);
        if (canvas == NULL)
        {
                fprintf(stderr, "Error: NULL canvas in draw_top.\n");
                return ERR;
        }
        if (move(offset_y, offset_x) == ERR)
        {
                fprintf(stderr, "Error: move failed in draw_top.\n");
                return ERR;
        }
        if (printw("%s (%lu x %lu)", realname, canvas->width, canvas->height) ==
            ERR)
        {
                fprintf(stderr, "Error: printw failed in draw_top.\n");
                return ERR;
        }
        return OK;
}

unsigned char
get_bit(byte, index)
unsigned char byte;
unsigned char index;
{
        return byte & (1 << index);
}

int
draw_bit(bit, cursor, hot)
unsigned char bit;
bool cursor;
bool hot;
{
        if (cursor)
        {
                if (attron(CURSOR_ATTR) == ERR)
                {
                        fprintf(stderr, "Error: attron failed in draw_bit.\n");
                        return XBM_ERR;
                }
        }
        if (hot)
        {
                if (attron(HOT_ATTR) == ERR)
                {
                        fprintf(stderr, "Error: attron failed in draw_bit.\n");
                        return XBM_ERR;

                }
        }
        if (bit)
        {
                if (addch(FULL_CHAR) == ERR)
                {
                        fprintf(stderr, "Error: addch failed in draw_bit.\n");
                        return XBM_ERR;
                }
        }
        else
        {
                if (addch(EMPTY_CHAR) == ERR)
                {
                        fprintf(stderr, "Error: addch failed in draw_bit.\n");
                        return XBM_ERR;
                }
        }
        if (cursor)
        {
                if (attroff(CURSOR_ATTR) == ERR)
                {
                        fprintf(stderr,
                                "Error: attroff failed in draw_bit.\n");
                        return XBM_ERR;
                }
        }
        if (hot)
        {
                if (attroff(HOT_ATTR) == ERR)
                {
                        fprintf(stderr,
                                "Error: attroff failed in draw_bit.\n");
                        return XBM_ERR;

                }
        }
        return XBM_OK;
}

int
draw_canvas(canvas, cursor, offset_x, offset_y)
xbm_file *canvas;
struct coord *cursor;
size_t offset_x;
size_t offset_y;
{
        size_t curr_y;
        unsigned char bit_i = 0;
        size_t arr_i = 0;
        if (canvas == NULL || cursor == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in draw_canvas.\n");
                return ERR;
        }
        for (curr_y = 0; curr_y < canvas->height; ++curr_y)
        {
                size_t curr_x;
                move(offset_y, offset_x);
                for (curr_x = 0; curr_x < canvas->width; ++curr_x)
                {
                        if (arr_i >= canvas->length)
                        {
                                fprintf(stderr, "Error: Too many bits in "
                                                "draw_canvas.\n");
                                return ERR;
                        }
                        if (draw_bit(get_bit(canvas->bits[arr_i], bit_i),
                                     cursor->x_pos == curr_x &&
                                     cursor->y_pos == curr_y,
                                     canvas->has_hot &&
                                     canvas->x_hot == curr_x &&
                                     canvas->y_hot == curr_y)
                            == XBM_ERR)
                        {
                                fprintf(stderr,
                                        "Error: draw_bit failed in "
                                        "draw_canvas.\n");
                        }
                        ++bit_i;
                        if (bit_i >= 8)
                        {
                                bit_i = 0;
                                ++arr_i;
                        }
                }
                if (bit_i != 0)
                {
                        bit_i = 0;
                        ++arr_i;
                }
                ++offset_y;
        }
        return OK;
}

int
draw_mesg(mesg, offset_x, offset_y)
char mesg[];
size_t offset_x;
size_t offset_y;
{
        if (mesg != NULL)
        {
                if (move(offset_y, offset_x) == ERR)
                {
                        fprintf(stderr, "Error: move failed in draw_mesg.\n");
                        return ERR;
                }
                if (printw("%s", mesg) == ERR)
                {
                        fprintf(stderr, "Error: printw failed in "
                                        "draw_mesg.\n");
                        return ERR;
                }
        }
        return OK;
}

int
edit_draw(canvas, filename, cursor, mesg)
xbm_file *canvas;
char filename[];
struct coord *cursor;
char mesg[];
{
        int status = OK;
        if (canvas == NULL ||
            cursor == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in edit_draw.\n");
                return XBM_ERR;
        }
        status = erase();
        status = draw_top(filename, canvas, 0, 0);
        status = draw_canvas(canvas, cursor, 0, 1);
        status = draw_mesg(mesg, 0, canvas->height + 1);
        status = refresh();
        if (status == ERR)
        {
                fprintf(stderr, "Error: failed to draw in edit_draw.\n");
                return XBM_ERR;
        }
        return XBM_OK;
}

void
edit_up(cursor)
struct coord *cursor;
{
        if (cursor != NULL && cursor->y_pos > 0)
        {
                --cursor->y_pos;
        }
}

void
edit_down(cursor, height)
struct coord *cursor;
size_t height;
{
        if (cursor != NULL && cursor->y_pos + 1 < height)
        {
                ++cursor->y_pos;
        }
}

void
edit_left(cursor)
struct coord *cursor;
{
        if (cursor != NULL && cursor->x_pos > 0)
        {
                --cursor->x_pos;
        }
}

void
edit_right(cursor, width)
struct coord *cursor;
size_t width;
{
        if (cursor != NULL && cursor->x_pos + 1 < width)
        {
                ++cursor->x_pos;
        }
}

void
flip_bit(canvas, cursor)
xbm_file *canvas;
struct coord *cursor;
{
        if (canvas != NULL && cursor != NULL && canvas->bits != NULL)
        {
                size_t real_width = 8 * (canvas->width / 8 + (canvas->width & 7 ? 1 : 0));
                size_t index = real_width * cursor->y_pos + cursor->x_pos;
                size_t bit_i = index & 7;
                size_t byte_i = index / 8;
                canvas->bits[byte_i] = canvas->bits[byte_i] ^ (1 << bit_i);
        }
}

void
edit_h(canvas, cursor, mesg_ptr)
xbm_file *canvas;
struct coord *cursor;
char **mesg_ptr;
{
        if (canvas != NULL && cursor != NULL && mesg_ptr != NULL)
        {
                if (canvas->x_hot != cursor->x_pos ||
                    canvas->y_hot != cursor->y_pos)
                {
                        canvas->x_hot = cursor->x_pos;
                        canvas->y_hot = cursor->y_pos;
                        canvas->has_hot = true;
                        *mesg_ptr = "Hotspot updated.";
                }
                else
                {
                        canvas->has_hot = false;
                        *mesg_ptr = "Hotspot removed.";
                }
        }
}

int
edit_s(canvas, filename, mesg_ptr)
xbm_file *canvas;
char filename[];
char **mesg_ptr;
{
        if (!xbm_save(canvas, filename))
        {
                fprintf(stderr,
                        "xbm_save failed in edit_s.\n");
                *mesg_ptr = "Save failed.";
                return XBM_ERR;
        }
        *mesg_ptr = "Saved.";
        return XBM_OK;
}

int
edit_a(canvas, filename_ptr, mesg_ptr)
xbm_file *canvas;
char **filename_ptr;
char **mesg_ptr;
{
        char fn_buf[FN_BUF_MAX];
        char *tempname;
        if (canvas == NULL ||
            filename_ptr == NULL ||
            canvas->bits == NULL ||
            mesg_ptr == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in edit_a.\n");
                *mesg_ptr = "Save as failed.";
                return XBM_ERR;
        }
        tempname = *filename_ptr;
        if (move(canvas->height + 1, 0) == ERR)
        {
                fprintf(stderr, "Error: move failed in edit_a.\n");
                *mesg_ptr = "Save as failed.";
                return XBM_ERR;
        }
        if (printw("Save to: ") == ERR)
        {
                fprintf(stderr, "Error: printw failed in edit_a.\n");
                *mesg_ptr = "Save as failed.\n";
                return XBM_ERR;
        }
        echo();
        if (getnstr(fn_buf, FN_BUF_MAX) == ERR)
        {
                fprintf(stderr, "Error: getnstr failed in edit_a.\n");
                *mesg_ptr = "Save as failed.\n";
                return XBM_ERR;
        }
        noecho();
        *filename_ptr = strdup(fn_buf);
        if (*filename_ptr == NULL)
        {
                fprintf(stderr, "Error: strdup failed in edit_a.\n");
                *mesg_ptr = "Save as failed.";
                return XBM_ERR;
        }
        if (!xbm_save(canvas, *filename_ptr))
        {
                fprintf(stderr, "xbm_save failed in edit_a.\n");
                *mesg_ptr = "Save as failed.";
                free(*filename_ptr);
                *filename_ptr = tempname;
                return XBM_ERR;
        }
        *mesg_ptr = "Saved as.";
        return XBM_OK;
}

int
edit_update(canvas, filename_ptr, cursor, mesg_ptr)
xbm_file *canvas;
char **filename_ptr;
struct coord *cursor;
char **mesg_ptr;
{
        int in_key = getch();
        *mesg_ptr = "";
        if (canvas == NULL ||
            filename_ptr == NULL ||
            cursor == NULL ||
            mesg_ptr == NULL)
        {
                fprintf(stderr, "Error: NULL parameters in edit_update.\n");
                return XBM_ERR;
        }
        switch (in_key)
        {
                case KEY_UP:
                        edit_up(cursor);
                        break;
                case KEY_DOWN:
                        edit_down(cursor, canvas->height);
                        break;
                case KEY_LEFT:
                        edit_left(cursor);
                        break;
                case KEY_RIGHT:
                        edit_right(cursor, canvas->width);
                        break;
                case 'q':
                        return XBM_OK;
                        break;
                case ' ':
                        flip_bit(canvas, cursor);
                        break;
                case 'h':
                        edit_h(canvas, cursor, mesg_ptr);
                        break;
                case 's':
                        edit_s(canvas, *filename_ptr, mesg_ptr);
                        break;
                case 'a':
                        if (edit_draw(canvas, *filename_ptr, cursor, NULL) ==
                            XBM_ERR)
                        {
                                fprintf(stderr, "Error: edit_draw failed in "
                                                "edit_update.\n");
                                *mesg_ptr = "Save as failed.";
                                return XBM_ERR;
                        }
                        edit_a(canvas, filename_ptr, mesg_ptr);
                        break;
        }
        return XBM_CONT;
}

int
edit_loop(canvas, filename)
xbm_file *canvas;
char filename[];
{
        struct coord cursor = {0, 0};
        int update_status = XBM_CONT;
        char *mesg = NULL;
        int draw_status = edit_draw(canvas, filename, &cursor, mesg); /* TODO */
        while (update_status == XBM_CONT)
        {
                update_status = edit_update(canvas, &filename, &cursor, &mesg); /* TODO */
                if (update_status == XBM_ERR)
                {
                        fprintf(stderr, "Error: edit_update failed in "
                                        "edit_loop.\n");
                        return XBM_ERR;
                }
                draw_status = edit_draw(canvas, filename, &cursor, mesg);
                if (draw_status == XBM_ERR)
                {
                        fprintf(stderr, "Error: edit_draw failed in edit_loop.\n");
                        return XBM_ERR;
                }
        }
        return XBM_OK;;
}

int
main(argc, argv)
int argc;
char **argv;
{
        xbm_file *canvas;
        char *filename = NULL;
        if (argc == 1)
        {
                canvas = init_prompt();
        }
        else if (argc == 2)
        {
                canvas = xbm_open(argv[1]);
                filename = argv[1];
        }
        else
        {
                fprintf(stderr, "Usage: %s [filename]\n", argv[0]);
                return EXIT_FAILURE;
        }
        if (canvas == NULL)
        {
                fprintf(stderr, "Error: Failed to initialise canvas.\n");
                return EXIT_FAILURE;
        }
        if (init_curses() == XBM_ERR)
        {
                fprintf(stderr, "Error: init_curses failed in main.\n");
                free_xbm(canvas);
                return EXIT_FAILURE;
        }
        if (edit_loop(canvas, filename) == XBM_ERR)
        {
                fprintf(stderr, "Error: edit_loop failed in main.\n");
                return EXIT_FAILURE;
        }
        free_xbm(canvas);
        if (kill_curses() == XBM_ERR)
        {
                fprintf(stderr, "Error: kill_curses failed in main.\n");
                return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
}
